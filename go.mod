module "gitlab.com/ladydascalie/pixelgame"

require (
	"github.com/faiface/beep" v0.0.0-20180303161934-4ec0906f4187
	"github.com/faiface/glhf" v0.0.0-20170610131041-98c0391c0fd3
	"github.com/faiface/mainthread" v0.0.0-20171120011319-8b78f0a41ae3
	"github.com/faiface/pixel" v0.7.0
	"github.com/go-gl/gl" v0.0.0-20180319200113-f8cee0ff968a
	"github.com/go-gl/glfw" v0.0.0-20170814180746-513e4f2bf85c
	"github.com/go-gl/mathgl" v0.0.0-20180319210751-5ab0e04e1f55
	"github.com/golang/freetype" v0.0.0-20170609003504-e2365dfdc4a0
	"github.com/hajimehoshi/go-mp3" v0.0.0-20180207144744-3c185f92b8db
	"github.com/hajimehoshi/oto" v0.0.0-20180325180608-2e4fbedc591b
	"github.com/pkg/errors" v0.8.0
	"golang.org/x/image" v0.0.0-20180314180248-f3a9b89b59de
)
