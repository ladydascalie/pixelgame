package script

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"path/filepath"
	"strings"

	"gitlab.com/ladydascalie/pixelgame/config"
)

// NewScript returns a new script, loaded from file.
func NewScript() (s *Script) {
	fname := filepath.Join(config.GetProjectRoot(), "script/script.json")
	b, err := ioutil.ReadFile(fname)
	if err != nil {
		log.Fatalf("cannot read script file: %v", err)
	}
	if err := json.Unmarshal(b, &s); err != nil {
		log.Fatalf("cannot parse script file: %v", err)
	}
	s.CurrentNode = s.Story[0]
	return s
}

// Script defines a full conversation node
type Script struct {
	Story       []*Story  `json:"story"`
	Endings     []*Ending `json:"endings"`
	CurrentNode *Story
}

var replacers = []string{"[[player]]"}

// Placeholders checks if elements needs to be replaced inside a given string
func (s *Story) Placeholders() {
	switch s.From {
	case "[[game]]":
		s.From = "Game Message"
	case "[[player]]":
		s.From = config.PlayerName
	}
	for _, repl := range replacers {
		if strings.Contains(s.Text, repl) {
			fmt.Println(s.Text)
			s.Text = strings.Replace(s.Text, repl, config.PlayerName, -1)
			fmt.Println(s.Text)
		}
	}
}

// Ending defines the endings
type Ending struct {
	Index string `json:"index"`
	From  string `json:"from"`
	Text  string `json:"text"`
}

// Story is a block of content which contains text, and possibly an event.
type Story struct {
	Index    string  `json:"index"`
	Goto     string  `json:"goto"`
	From     string  `json:"from"`
	Text     string  `json:"text"`
	IsSplash bool    `json:"is_splash"`
	Events   []Event `json:"event"`
}

// Goto fetches a node by it's name
func (s *Script) Goto(index string) {
	for _, story := range s.Story {
		if story.Index == index {
			story.Placeholders()
			s.CurrentNode = story
		}
	}
}

// Events defines the content that an event can hold
type Event struct {
	Scenery    string   `json:"scenery"`
	Characters []string `json:"characters"`
	Processed  bool
}
