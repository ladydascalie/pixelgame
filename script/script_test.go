package script

import (
	"testing"

	"github.com/davecgh/go-spew/spew"
)

func TestFull(t *testing.T) {
	s := NewScript()
	s.Goto("begin.2")
	spew.Dump(s.CurrentNode)
}
