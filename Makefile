all: run
run:
	go build && ./pixelgame
vendor: init ensure
init:
	dep init
ensure:
	dep ensure -v
