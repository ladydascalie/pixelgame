package game

import (
	"io/ioutil"
	"log"
	"path/filepath"
	"strings"

	"fmt"
	"strconv"

	"github.com/faiface/pixel"
)

// initCharacterMap returns a map of string to characters.
// the sprites are loaded from the chara folder.
func initCharacterMap() map[string]*character {
	// load all characters
	const charPath = "assets/img/chara"
	var charMap = make(map[string]*character)

	files, err := ioutil.ReadDir(charPath)
	if err != nil {
		log.Fatal(err)
	}

	// This effectively means that we now have an atlas of sprites.
	// They get loaded on game init, and do not get unloaded until the game quits.
	for _, f := range files {
		name := strings.TrimSuffix(f.Name(), filepath.Ext(f.Name()))
		charMap[name] = &character{
			Sprite: spriteFromPicture(filepath.Join(charPath, f.Name())),
			Align:  alignCenter, // this is just a default.
		}
	}
	return charMap
}

// initBackgroundMap returns a map of string to pixel.Sprite
// the sprites are loaded from the bg folder.
func initBackgroundMap() map[string]*pixel.Sprite {
	// load all backgrounds
	const bgPath = "assets/img/bg"
	var bgMap = make(map[string]*pixel.Sprite)

	files, err := ioutil.ReadDir(bgPath)
	if err != nil {
		log.Fatal(err)
	}

	for _, f := range files {
		name := strings.TrimSuffix(f.Name(), filepath.Ext(f.Name()))
		bgMap[name] = spriteFromPicture(filepath.Join(bgPath, f.Name()))
	}
	return bgMap
}

// logMousePos returns a nicely formatted display of the current mouse vector.
func logMousePos(g *Game) {
	// get mouse pos
	pos := g.Window.MousePosition()
	x, y := pos.XY()

	// print
	fmt.Println("----- MOUSE POSITION -----")
	fmt.Printf("%+#v\n", pos)                       // regular pixel.Vec print
	fmt.Printf("pixel.V(%.2f, %.2f)\n", x, y)       // as a pixel.V() call
	fmt.Printf("pixel.V(%s, %s)\n", f64(x), f64(y)) // as a pixel.V() call the least 0 possible
	fmt.Printf("pixel.V(%.0f, %.0f)\n", x, y)       // as a pixel.V() call with truncated values
	fmt.Println("----- MOUSE POSITION -----")
}

// f64 returns a float formatted to the least significant 0.
func f64(f float64) string {
	return strconv.FormatFloat(f, 'f', -1, 64)
}
