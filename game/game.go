package game

import (
	"fmt"
	"image"
	_ "image/jpeg" // for decoding
	_ "image/png"  // for decoding
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"github.com/faiface/beep"
	"github.com/faiface/beep/speaker"
	"github.com/faiface/pixel"
	"github.com/faiface/pixel/imdraw"
	"github.com/faiface/pixel/pixelgl"
	"github.com/faiface/pixel/text"
	"github.com/golang/freetype/truetype"
	"gitlab.com/ladydascalie/pixelgame/config"
	"gitlab.com/ladydascalie/pixelgame/script"
	"golang.org/x/image/colornames"
	"golang.org/x/image/font"
)

var roboto font.Face

func init() {
	target := filepath.Join(config.GetProjectRoot(), "assets/fonts/RobotoMono-Regular.ttf")
	f, err := os.Open(target)
	if err != nil {
		log.Fatal(err)
	}
	b, err := ioutil.ReadAll(f)
	if err != nil {
		log.Fatal(err)
	}
	tt, err := truetype.Parse(b)

	const dpi = 72
	const fontSize = 24
	roboto = truetype.NewFace(tt, &truetype.Options{
		Size:              fontSize,
		DPI:               dpi,
		Hinting:           font.HintingFull,
		GlyphCacheEntries: 1,
	})
	defer f.Close()
}

// Game defines the global game structure.
type Game struct {
	Window         *pixelgl.Window
	TextAtlas      *text.Atlas
	CharacterAtlas map[string]*character
	BGAtlas        map[string]*pixel.Sprite
	Script         *script.Script
	DialogBox      DialogBox
}

// DialogBox defines both the message to be displayed on screen,
// and the speaker, ex. "Hello World" from "Foobar".
type DialogBox struct {
	Message *text.Text
	Speaker *text.Text
}

// Clear the whole dialog box.
func (d *DialogBox) Clear() {
	d.Message.Clear()
	d.Speaker.Clear()
}

// Draw the entire dialog box at once.
func (d *DialogBox) Draw(win *pixelgl.Window, matrix pixel.Matrix) {
	d.Speaker.Draw(win, matrix)
	d.Message.Draw(win, matrix)
}

// NewGame returns a new Game with default settings.
func NewGame() (game *Game) {
	// make the base game object
	game = &Game{
		TextAtlas:      text.NewAtlas(roboto, text.ASCII),
		CharacterAtlas: initCharacterMap(),
		BGAtlas:        initBackgroundMap(),
		Script:         script.NewScript(),
	}

	// make the dialog box
	dialogBox := DialogBox{
		Message: text.New(pixel.V(45, 100), game.TextAtlas),
		Speaker: text.New(pixel.V(45, 134), game.TextAtlas),
	}

	// add the dialog box to the game
	game.DialogBox = dialogBox
	return
}

// Run the main game loop.
func (g *Game) Run() {
	// setting up the window HAS to be done in Run()
	// the rest of the dependencies are fine to to in NewGame()
	cfg := pixelgl.WindowConfig{
		Title:  "Dunewalker Dating Sim",
		Bounds: pixel.R(0, 0, 1024, 768),
	}
	win, err := pixelgl.NewWindow(cfg)
	if err != nil {
		panic(err)
	}
	win.SetSmooth(true)         // set smoothing
	win.Clear(colornames.Black) // set default bg color
	g.Window = win              // set the window on the game

	// todo: move all the music things onto Game directly (perhaps make an AudioPlayer object)
	// load the music file
	s, format := audioStreamFromFile("assets/music/day.mp3")
	// set it to play infinitely
	beep.Loop(-1, s)
	speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/10))
	speaker.Play(s) // start playing

	g.advance()

	// set the framerate at 20 fps.
	fps := time.Tick(time.Second / 20)

	// main game loop
	// then this exits, the whole game exits.
	for !g.Window.Closed() {
		switch {
		case g.Window.JustPressed(pixelgl.KeySpace):
			g.advance()
		case g.Window.JustPressed(pixelgl.MouseButtonLeft):
			tr := pixel.V(615, 350)
			bl := pixel.V(410, 275)

			minX, minY := bl.XY()
			maxX, maxY := tr.XY()

			btn := NewButton("", pixel.R(minX, minY, maxX, maxY))

			if btn.Rect.Contains(g.Window.MousePosition()) {
				fmt.Println("YES")
				break
			}
			fmt.Println("NOPE")
		case g.Window.JustPressed(pixelgl.MouseButtonRight):
			logMousePos(g)
		case g.Window.JustPressed(pixelgl.KeyGraveAccent):
			log.Println(s.Position())
		}
		g.Window.Update()
		<-fps // read from the channel
	}
}

// Advance is the meat of the game loop.
func (g *Game) advance() {
	// special case for splash screens...
	if g.Script.CurrentNode.IsSplash {
		g.handleSplash()
		//const uiPath = "assets/img/ui"
		//btn := spriteFromPicture(filepath.Join(uiPath, "btn_test.png"))
		//btn.Draw(g.Window, pixel.IM.Moved(g.Window.Bounds().Center()))
		return
	}

	// Get the message and msgFrom
	message := g.Script.CurrentNode.Text
	msgFrom := strings.Title(g.Script.CurrentNode.From)

	// then incrementally draw the message
	var tmp string
	for _, r := range message {
		// clear the message box
		g.DialogBox.Clear()
		// Build and print the message
		tmp += string(r)

		// Wait between each character
		time.Sleep(config.TextSpeed)

		// Draw
		g.Window.Clear(colornames.Black)

		// process events
		processEventNodes(g)

		// draw dialog box
		drawDialogBox(g.Window)

		// draw message box
		drawSpeakerBox(g.Window, g.Script.CurrentNode.From)

		drawBox(g.Window)

		// Add the message text
		fmt.Fprintln(g.DialogBox.Message, tmp)

		// Add the speaker text
		fmt.Fprintln(g.DialogBox.Speaker, msgFrom)

		// Draw the speaker box
		g.DialogBox.Draw(g.Window, pixel.IM)

		// Update the window
		g.Window.Update()
	}
	g.Script.Goto(g.Script.CurrentNode.Goto)
}

// processEventNodes processes all events provided in a node.
func processEventNodes(g *Game) {
	// this sync.Once is being used to
	// prevent drawing backgrounds more than once
	// even if multiple characters are present on screen.
	var once sync.Once
	// iterate through the events for the current script node.
	for _, event := range g.Script.CurrentNode.Events {
		// draw the backgrounds only once
		once.Do(func() {
			bg, ok := g.BGAtlas[event.Scenery]
			if !ok {
				log.Fatalf("missing or invalid background: %v", event.Scenery)
			}
			bg.Draw(g.Window, pixel.IM.Moved(g.Window.Bounds().Center()))
		})

		// draw characters. this will happen only if there are
		// any characters to draw, as defined in the current nodes events.
		for _, characterString := range event.Characters {
			name, position := decodeCharacterString(characterString)

			// get the character from the atlas
			chara, ok := g.CharacterAtlas[name]
			if !ok { // invalid options given should result in a failure.
				log.Fatalf("invalid characterString: %v", name)
			}
			old := g.Window.Bounds().Center()
			pos := chara.alignTo(position, old)
			chara.Draw(g.Window, pixel.IM.Moved(pos))
			continue
		}
	}
}

// handleSplash draws a splash screen.
func (g *Game) handleSplash() {
	bg, ok := g.BGAtlas[g.Script.CurrentNode.Events[0].Scenery]
	if !ok {
		log.Fatalf("splash must have an associated background")
	}
	bg.Draw(g.Window, pixel.IM.Moved(g.Window.Bounds().Center()))
	g.Script.Goto(g.Script.CurrentNode.Goto)
}

// spriteFromPicture uses mustLoadPicture and will error fatally
// if the picture could not be loaded.
func spriteFromPicture(path string) *pixel.Sprite {
	data := mustLoadPicture(path)
	return pixel.NewSprite(data, data.Bounds())
}

// mustLoadPicture wraps loadPicture and will error fatally
// if the picture could not be loaded.
func mustLoadPicture(path string) pixel.Picture {
	p, err := loadPicture(path)
	if err != nil {
		log.Fatalln(err)
	}
	return p
}

// loadPicture returns a pixel picture from the path on disk
func loadPicture(path string) (pixel.Picture, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	img, _, err := image.Decode(file)
	if err != nil {
		return nil, err
	}
	return pixel.PictureDataFromImage(img), nil
}

func drawBox(win *pixelgl.Window) {
	box := imdraw.New(nil)
	box.EndShape = imdraw.RoundEndShape
	box.Color = pixel.RGBA{R: 0, G: 0, B: 0, A: .6}
	box.Push(pixel.V(615, 350))

	box.Color = pixel.RGBA{R: 0, G: 0, B: 0, A: .6}
	box.Push(pixel.V(410, 275))

	box.Rectangle(0)
	box.Draw(win)
}

// drawDialogBox...
// draws a dialog box to the screen...
func drawDialogBox(win *pixelgl.Window) {
	diag := imdraw.New(nil)

	diag.Color = pixel.RGBA{R: 0, G: 0, B: 0, A: .6}
	diag.Push(pixel.V(40, 20))

	diag.Color = pixel.RGBA{R: 0, G: 0, B: 0, A: .6}
	diag.Push(pixel.V(984, 120))

	diag.Rectangle(0)

	diag.Draw(win)

	border := imdraw.New(nil)

	border.Color = pixel.RGBA{R: 1, G: .42, B: .7, A: .6}
	border.Push(pixel.V(40, 20))

	border.Color = pixel.RGBA{R: 1, G: .42, B: .7, A: .6}
	border.Push(pixel.V(984, 120))

	border.Rectangle(2)

	border.Draw(win)
}

// drawSpeakerBox...
// draws a simple box, it's size adapted to the
// text which much be displayed on top of it.
// think of 2 vectors like this:
// UPPER-LEFT: 40     |    BOTTOM LEFT: 20
// BOTTOM RIGHT: 984  |    UPPER RIGHT: 120
//  +-----------------+
//  |       |984 ⬈ 120|
//  +-----------------+
//  |40 ⬋ 20|         |
//  +-----------------+
func drawSpeakerBox(win *pixelgl.Window, from string) {
	width := measureText(from, roboto)
	shape := imdraw.New(nil)
	shape.Color = pixel.RGBA{R: 0, G: 0, B: 0, A: .6}
	shape.Push(pixel.V(40, 126))
	shape.Color = pixel.RGBA{R: 0, G: 0, B: 0, A: .6}
	shape.Push(pixel.V(float64(width+60), 154))
	shape.Rectangle(0)
	shape.Draw(win)

	border := imdraw.New(nil)
	border.Color = pixel.RGBA{R: 1, G: .42, B: .7, A: .6}
	border.Push(pixel.V(40, 126))
	border.Color = pixel.RGBA{R: 1, G: .42, B: .7, A: .6}
	border.Push(pixel.V(float64(width+60), 154))
	border.Rectangle(2)
	border.Draw(win)

}

// measureText measures the size of a string with a given font face.
// This is probably best computed the fewest amount of times
// necessary, use measureText for convenience.
func measureText(text string, face font.Face) int {
	bounds, _ := font.BoundString(face, text)
	return (bounds.Max.X - bounds.Min.X).Ceil()
}
