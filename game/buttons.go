package game

import "github.com/faiface/pixel"

//Button defines a button.
type Button struct {
	Text string
	Rect pixel.Rect
}

//NewButton returns a new button with the given properties
func NewButton(text string, rect pixel.Rect) *Button {
	return &Button{
		Text: text,
		Rect: rect,
	}
}
