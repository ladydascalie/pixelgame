package game

import (
	"fmt"
	"strings"

	"github.com/faiface/pixel"
)

const (
	alignLeft   align = "left"
	alignRight  align = "right"
	alignCenter align = "center"
)

// align defines an enum of valid alignment values.
type align string

// String satisfies the Stringer interface.
func (a align) String() string {
	return string(a)
}

// Valid checks if a given position is valid.
func isValidAlignment(position string) bool {
	allowed := []align{alignLeft, alignRight, alignCenter}
	for _, value := range allowed {
		if position == value.String() {
			return true
		}
	}
	return false
}

// character defines a character.
//
// A sprite is embedded, for working with pixel,
// and an alignment is defined.
type character struct {
	*pixel.Sprite // embed sprite
	Name          string
	Align         align
}

// decodeCharacterString a character by it's name in the script.
//
// decodeCharacterString may panic if given a character name and position which cannot be processed.
func decodeCharacterString(character string) (name, position string) {
	parts := strings.SplitN(character, ".", 2)
	switch len(parts) {
	case 1:
		name, position = parts[0], alignCenter.String()
		return
	case 2:
		name, position = parts[0], parts[1]
		if isValidAlignment(position) {
			return
		}
	}
	msg := fmt.Sprintf("unsupported arguments to character map: %v\n", parts)
	panic(msg)
}

// AlignTo will align to the provided alignment.
// a panic may occur if an invalid value is passed.
func (c *character) alignTo(position string, old pixel.Vec) pixel.Vec {
	switch position {
	case alignLeft.String():
		return c.alignLeft(old)
	case alignRight.String():
		return c.alignRight(old)
	case alignCenter.String():
		return c.alignCenter(old)
	}
	msg := fmt.Sprintf("invalid argument: %v", position)
	panic(msg)
}

// alignLeft returns a vector compatible struct
// of positions for left alignment.
func (c *character) alignLeft(old pixel.Vec) pixel.Vec {
	return pixel.Vec{
		X: old.X + c.Picture().Bounds().W()/2 - c.Picture().Bounds().W(),
		Y: c.Picture().Bounds().H() - c.Picture().Bounds().H()/2,
	}
}

// alignRight returns a vector compatible struct
// of positions for right alignment.
func (c *character) alignRight(old pixel.Vec) pixel.Vec {
	return pixel.Vec{
		X: old.X + c.Picture().Bounds().W()*1.5 - c.Picture().Bounds().W(),
		Y: c.Picture().Bounds().H() - c.Picture().Bounds().H()/2,
	}
}

// alignCenter returns a vector compatible struct
// of positions for center alignment.
func (c *character) alignCenter(old pixel.Vec) pixel.Vec {
	return pixel.Vec{
		X: old.X + c.Picture().Bounds().W() - c.Picture().Bounds().W(),
		Y: c.Picture().Bounds().H() - c.Picture().Bounds().H()/2,
	}
}
