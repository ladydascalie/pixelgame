package game

import (
	"log"
	"os"

	"github.com/faiface/beep"
	"github.com/faiface/beep/mp3"
)

func rewind(s beep.StreamSeekCloser) {
	// check and handle the audio
	//if s.Position() == s.Len()-1 {
	//	spew.Dump("in position check")
	//	if err := s.Seek(0); err != nil {
	//		log.Fatalf("error rewinding audio player: %v", err)
	//	}
	//	speaker.Play()
	//}
}

func audioStreamFromFile(path string) (stream beep.StreamSeekCloser, format beep.Format) {
	f, err := os.Open(path)
	if err != nil {
		log.Fatalln(err)
	}
	// decode it
	s, format, err := mp3.Decode(f)
	if err != nil {
		log.Fatalln(err)
	}
	return s, format
}
