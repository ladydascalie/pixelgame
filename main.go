package main // import "gitlab.com/ladydascalie/pixelgame"

import (
	"log"

	"github.com/faiface/pixel/pixelgl"
	"gitlab.com/ladydascalie/pixelgame/game"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	// fire up the game
	g := game.NewGame()
	pixelgl.Run(g.Run)
}
