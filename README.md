# Pixel Game

## Setup

Best to view the requirements from the game engine I used:

- [faiface/pixel](https://github.com/faiface/pixel#requirements)

On macOS, you need Xcode or Command Line Tools for Xcode (`xcode-select --install`) for required headers and libraries.

## Running the game

```shell
# This project requires that you have vgo installed:
go get -u golang.org/x/vgo

# Then simply use the makefile
make
```
