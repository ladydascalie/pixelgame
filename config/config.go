package config

import (
	"path/filepath"
	"runtime"
	"strings"
	"time"
)

var (
	// DefaultPlayerName defines the default that a player will be given
	DefaultPlayerName string
	// PlayerName will be configured through user input.
	// if it is empty, then DefaultPlayerName should be used
	PlayerName string
	// TextSpeed defines the speed at which text should be displayed on screen
	TextSpeed time.Duration

	// DefaultTextSpeed defines the speed that is used when no settings are changed.
	DefaultTextSpeed = 5 * time.Millisecond
)

func init() {
	DefaultPlayerName = "[Generic Main Character]"
	if PlayerName == "" {
		PlayerName = DefaultPlayerName
	}
	TextSpeed = DefaultTextSpeed
}

// GetProjectRoot returns the root of the project.
// useful for concatenating file paths
func GetProjectRoot() string {
	// this will be trimmed away each time
	const target = "config"
	_, b, _, _ := runtime.Caller(0)
	return strings.TrimSuffix(filepath.Dir(b), target)
}
